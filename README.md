there are 3 versions in 3 different branches
1. Local: please switch to the local branch:
-         to see the code switch to the local_app branch
-         to run create e virtual env with requirements.txt 
-         to access the front page: run: 
-             cd front; yarn start
-         to access the backend: run: 
-             cd back; make run-db; uvicorn index:app --reload

2. dockerized version:
-         to see the code switch to the dockerized branch
-         run docker-compose up

3. aws
-         this part is not fully integrated and completed, however the codes used can be found at folder terraform_codes
