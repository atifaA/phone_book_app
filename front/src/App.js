import {AppBar, Toolbar, Button, Box, TableContainer, Table, TableBody, TableRow, TableCell, TextField } from '@material-ui/core/';
import {useState} from 'react';
import axios from 'axios'

function App() {
  const [users, setUsers] = useState([])
  const [user, setUser] = useState({})
  const fetchUsers = async () =>{
    const response =  await axios.get('http://localhost:8000/')
    return setUsers(response.data)
  }

  const fetchUser = async (id) =>{
    const response =  await axios.get(`http://localhost:8000/${id}`)
    return setUser(response.data)
  }

  const CreateOrEditUser = async () =>{
    if(user.id){
      await axios.put(`http://localhost:8000/${user.id}`, user)
    }
    else{
      await axios.post(`http://localhost:8000/`, user)
    }
    await fetchUsers()
    await setUser({id: 0, name: '', phone: ''})
  }
  // const EditUser = async (id) =>{
  //   await axios.put(`http://localhost:8000/${id}`, user)
  //   await fetchUsers()
  //   await setUser({id: 0, name: '', phone: ''})
  // }
  const deleteUser = async (id) =>{
    await axios.delete(`http://localhost:8000/${id}`)
    await fetchUsers()
  }
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Button color="inherit">users</Button>
        </Toolbar>
      </AppBar>
      <Box m = {10}>
      <TableContainer>
        <TextField value={user.id} type="hidden" />
      <Table aria-label="simple table">
        <TableBody>
            <TableRow>
              <TableCell >
                <TextField value={user.name} onChange={(e) => setUser({...user, name:e.target.value})} id="standard-basic" label="Name" />
              </TableCell>
              <TableCell >
                <TextField value={user.phone} onChange={(e) => setUser({...user, phone:e.target.value})} id="standard-basic" label="phone" />
              </TableCell>
              <TableCell >
                <Button onClick={(e) => CreateOrEditUser()} variant="contained" color="primary">
                  Submit
                </Button>
              </TableCell>
              {/* <TableCell >
                <Button onClick={(e) => fetchUser(user.id)} variant="contained" color="primary">
                  Search
                </Button>
              </TableCell> */}

            </TableRow>
            <TableRow>
              <TableCell >Name</TableCell>
              <TableCell >phone</TableCell>
              <TableCell >Edit</TableCell>
              <TableCell >Delete</TableCell>
            </TableRow>
          {users.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.phone}</TableCell>
              <TableCell>
                <Button onClick={()=> CreateOrEditUser(row.id)} variant="contained" color="primary">
                  Edit
                </Button>
              </TableCell>
              <TableCell>
                <Button onClick={()=> deleteUser(row.id)} variant="contained" color="secondary">
                  Delete
                </Button>
              </TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
      </Box>

    </div>
  );
}

export default App;
