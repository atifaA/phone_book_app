from sqlalchemy import create_engine, MetaData

SQLALCHEMY_DATABASE_URL = "postgresql://postgres:mysuperpassword@localhost/youtube"


# SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://postgres:postgres@localhost/postgres"
# SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://postgres:postgres@postgres_db_container/postgres"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
meta = MetaData()
conn = engine.connect()
# conn.execute('set max_allowed_packet=67108864')

# from sqlalchemy import create_engine
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker


# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:mysuperpassword@localhost/youtube"

# engine = create_engine(SQLALCHEMY_DATABASE_URL)
# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Base = declarative_base()

# def get_db():
#     db = SessionLocal()
#     try:
#         yield db
#     except:
#         db.close()
