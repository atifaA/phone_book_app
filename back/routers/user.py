from typing import List
from fastapi import APIRouter

from models.user import users
from config.db import conn
from schemas.user import User

user = APIRouter()

@user.get("/",
        tags=["users"],
        description="Get a list of all users",)
async def list_users():
    return conn.execute(users.select()).fetchall()


@user.get("/users/{id}",
    tags=["users"],
    description="Get a single user by Id",)
async def get_user(id: int):
    return conn.execute(users.select().where(users.c.id == id)).first()


@user.post("/", tags=["users"],
           description="Create a new user")
async def create_users(user: User):
    conn.execute(users.insert().values(
        name = user.name,
        phone = user.phone,
        ))
    return conn.execute(users.select()).fetchall()


@user.put("/{id}", tags=["users"],
          description="Update a User by Id")
async def edit_user(id: int, user: User):
    conn.execute(users.update().values(
        name = user.name,
        phone = user.phone,
        ).where(users.c.id == id))
    return conn.execute(users.select()).fetchall()


@user.delete("/{id}", tags=["users"])
async def delete_user(id: int):
    conn.execute(users.delete().where(users.c.id == id))
    return conn.execute(users.select()).fetchall()