terraform {
    required_providers {
        aws =  {
            source = "hashicorp/aws" 
            version = "~> 3.0" 
        }
    }
}

provider "aws" {
    region = "us-east-1" 
    shared_credentials_file = "~/.aws/credentials"
    profile = "terraform-aws"
}

module "networks" {
  source = "./../modules/networks"
  availability_zones = var.availability_zones
}

output "cidr_block_front" {
    value = [for subnet in module.networks.front_subnets: subnet.cidr_block]
}

module "security" {
    source  = "./../modules/security"
    host_ip = var.HOST_IP
    vpc_id  = module.networks.vpc_id
    subnet_cidrs = {
        front = [for subnet in module.networks.front_subnets: subnet.cidr_block]
        back  = [for subnet in module.networks.back_subnets:  subnet.cidr_block]
        db = [for subnet in module.networks.database_subnets: subnet.cidr_block]
    }
}

module "loadbalancers" {
    source      = "./../modules/loadbalancers"
    step_vpc_id = module.networks.vpc_id
    subnet_ids  = {
        front = [for subnet in module.networks.front_subnets: subnet.id]
        back  = [for subnet in module.networks.back_subnets:  subnet.id]
    }
    security_group_ids = {
        front_alb = module.security.security_groups.front_alb_security_group.id
        back_alb  = module.security.security_groups.back_alb_security_group.id
    }
}

module "autoscaling" {
    source = "./../modules/autoscaling"
    aws_instance_type = "t2.micro"
    ami_id = ""

    key_pair_key_name = "task_se"
    security_group_ids = {
        web         = module.security.security_groups.web.id
        ssh         = module.security.security_groups.ssh.id 
        front_security_group = module.security.security_groups.front_security_group.id
        back_security_group  = module.security.security_groups.back_security_group.id
    }
    subnet_ids = {
      back = [ for subnet in module.networks.back_subnets: subnet.id ]
      front = [ for subnet in module.networks.front_subnets: subnet.id ]
    }
    availability_zones     = var.availability_zones
    step_target_group_arns = {
        front = module.loadbalancers.target_group_arns.front
        back  = module.loadbalancers.target_group_arns.back
    }
}


module "monitoring" {
    source = "./../modules/monitoring"
    autoscaling_group_names = {
        front = module.autoscaling.autoscaling_group_names.front
        back  = module.autoscaling.autoscaling_group_names.back
    }

    autoscaling_policy_arns = {
        front_scale_in  = module.autoscaling.autoscaling_policy_arns.front_scale_in
        front_scale_out = module.autoscaling.autoscaling_policy_arns.front_scale_out 
        back_scale_in   = module.autoscaling.autoscaling_policy_arns.back_scale_in
        back_scale_out  = module.autoscaling.autoscaling_policy_arns.back_scale_out
    }
}