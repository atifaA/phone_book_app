variable "HOST_IP" {
    type = string
    description = "IP Address of running host"
}

variable "region" {
    type = string
    description = "AWS VPC region"
}

variable "availability_zones" {
    type = list
    description = "AWS region availability zones"
}
