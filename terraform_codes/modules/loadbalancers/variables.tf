variable "step_vpc_id" {
    type = string
}
variable "subnet_ids" {
    type = object({
        front = list(string)
        back  = list(string)
    })
}

variable "security_group_ids" {
    type = object({
        front_alb = string
        back_alb  = string
    })
}