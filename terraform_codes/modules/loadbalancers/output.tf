output "target_group_arns" {
    value = {
        front = aws_load_balancer_target_group.front_target_group.arn
        back  = aws_load_balancer_target_group.back_target_group.arn
    }
}