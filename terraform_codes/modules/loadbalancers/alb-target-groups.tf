resource "aws_load_balancer_target_group" "front_target_group" {
	name     = "front-target_group"
	port     = 80
	protocol = "HTTP"
	vpc_id   = var.step_vpc_id	
}

resource "aws_load_balancer_target_group" "back_target_group" {
    name     = "back-target_group"
    port     = 80
    protocol = "HTTP"
    vpc_id   = var.step_vpc_id
}


