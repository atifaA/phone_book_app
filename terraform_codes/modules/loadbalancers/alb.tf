resource "aws_load_balancer" "front_load_balancer" {
    name = "front-load_balancer"
    internal = false

    load_balancer_type = "application"
    security_groups = [var.security_group_ids.front_alb]
    subnets =  var.subnet_ids.front

    tags = {
        Name = "front_load_balancer"
        Environment = "Production"
    }
}


resource "aws_load_balancer" "back_load_balancer" {
    name = "back-load_balancer"
    internal = true
    
    load_balancer_type = "application"
    security_groups = [var.security_group_ids.back_alb]
    subnets =  var.subnet_ids.back


    tags = {
        Name = "back_load_balancer"
        Environment = "Production"
    }
}


