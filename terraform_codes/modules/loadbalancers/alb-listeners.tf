resource "aws_load_balancer_listener" "front_listener" {
    load_balancer_arn = aws_load_balancer.front_load_balancer.arn
    port = 80
    protocol = "HTTP"

    default_action {
      type = "forward"
      target_group_arn = aws_load_balancer_target_group.front_target_group.arn
    }
}

resource "aws_load_balancer_listener" "back_listener" {
    load_balancer_arn = aws_load_balancer.back_load_balancer.arn 
    port = 80
    protocol = "HTTP"

    default_action {
      type = "forward"
      target_group_arn = aws_load_balancer_target_group.back_target_group.arn
    }
}