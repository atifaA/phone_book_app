output "security_groups" {
    value = {
        web = aws_security_group.web
        ssh = aws_security_group.ssh 
        front_alb_security_group = aws_security_group.front_alb_security_group
        front_security_group     = aws_security_group.front_security_group
        back_alb_security_group  = aws_security_group.back_alb_security_group
        back_security_group = aws_security_group.back_security_group
        database_security_group = aws_security_group.database_security_group
    }
}