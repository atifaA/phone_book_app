resource "aws_security_group" "web" {
    name = "allow_web"
    vpc_id = var.vpc_id
    ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # allows ssh connection
    tags = {
        Name = "allow web"
    }
}


resource "aws_security_group" "ssh" {
    name = "allow_ssh"
    vpc_id = var.vpc_id
    ingress {
        description = "Inbound ssh"
        from_port = 22
        to_port = 22
        protocol = "TCP"
        cidr_blocks = ["${var.host_ip}/32"]
    }

    tags = {
        Name = "allow ssh"
    }
}


// front

resource "aws_security_group" "front_alb_security_group" {
    name = "front_alb_security_group"
    vpc_id = var.vpc_id

    ingress {
        from_port = 433
        to_port = 433
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = var.subnet_cidrs.front
    }

    tags = {
        Name = "front alb security_group"
    }
}


resource "aws_security_group" "front_security_group" {
    name = "front_security_group"
    vpc_id = var.vpc_id

    ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        security_groups = [aws_security_group.front_alb_security_group.id]
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "front security_group"
    }
}

// back
resource "aws_security_group" "back_alb_security_group" {
    name = "back_alb_security_group"
    vpc_id = var.vpc_id

    ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = var.subnet_cidrs.front
    }

    egress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = var.subnet_cidrs.back
    }

    tags = {
        Name = "back alb security_group"
    }
}

resource "aws_security_group" "back_security_group" { #will be changed to applicatione4 load balancer
    name = "back_security_group"
    vpc_id = var.vpc_id

    ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        security_groups = [aws_security_group.back_alb_security_group.id]
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "back security_group"
    }
}

resource "aws_security_group" "database_security_group" {
    name = "database_security_group"
    vpc_id = var.vpc_id
    
    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "TCP"
        cidr_blocks = flatten([var.subnet_cidrs.back, var.subnet_cidrs.db])
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "database security_group"
    }
}