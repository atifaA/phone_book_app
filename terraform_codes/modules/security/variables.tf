variable "host_ip" {
  type = string
}

variable "vpc_id" {
  type = string
}
variable "subnet_cidrs" {
    type = object({
        front = list(string)
        back  = list(string)
        db       = list(string)
    })
}
