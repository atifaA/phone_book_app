// ================================================
// ----------------ROUTE TABLES--------------------
// ================================================
resource "aws_route_table" "public_front_rt" {
    vpc_id = aws_vpc.step_vpc.id

    # local routes are inherited
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw_front.id
    } 

    tags = {
        Name  = "PUBLIC-FRONTEND-RT"
        Group = "ROUTE-TABLES"
    }
}

resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.step_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_nat_gateway.nat_gw_private.id
    }

    tags = {
        Name  = "PRIVATE-RT"
        Group = "ROUTE-TABLES"
    }
}