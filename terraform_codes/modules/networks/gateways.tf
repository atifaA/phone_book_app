// ================================================
// ----------------GATEWAYS------------------------
// ================================================

// internet gateway for public subnets
resource "aws_internet_gateway" "igw_front" {
    vpc_id = aws_vpc.step_vpc.id
    
    tags = {
        Name  = "igw_front"
        Group = "gateways"
    }
}


// nat gateway for internet acces of private subnets
resource "aws_eip" "nat1" {
    depends_on = [aws_internet_gateway.igw_front]
}

resource "aws_nat_gateway" "nat_gw_private" {
  allocation_id = aws_eip.nat1.id
  subnet_id     = aws_subnet.public1.id

  tags = {
    Name  = "nat_gw_private"
    Group = "gateways"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw_front]
}