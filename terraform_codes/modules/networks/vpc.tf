// ===========================================
// -----------------VPC-----------------------
// ===========================================
resource "aws_vpc" "step_vpc" {
    instance_tenancy = "default"
    enable_dns_hostnames = true
    tags = {
        Name = "step_vpc"
    }
}