output "vpc_id" {
   value = aws_vpc.step_vpc.id
}
output "front_subnets" {
   value = [data.aws_subnet.public1_data, data.aws_subnet.public2_data] 
   description = "front Subnet ids"
}

output "back_subnets" {
   value = [data.aws_subnet.private1_1_data, data.aws_subnet.private1_2_data]
   description = "back Subnet ids"
}

output "database_subnets" {
   value = [data.aws_subnet.private2_1_data, data.aws_subnet.private2_2_data]
   description = "database Subnet ids"
}