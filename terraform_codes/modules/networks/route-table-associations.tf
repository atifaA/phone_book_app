// ================================================
// ----------------ROUTE TABLE ASSOCIATIONS--------------------
// ================================================
resource "aws_route_table_association" "public_association_1" {
    route_table_id = aws_route_table.private_rt.id
    subnet_id      = aws_subnet.public1.id
}

resource "aws_route_table_association" "public_association_2" {
    route_table_id = aws_route_table.private_rt.id
    subnet_id      = aws_subnet.public2.id 
}

resource "aws_route_table_association" "private_association_1_1" {
    route_table_id = aws_route_table.private_rt.id
    subnet_id      = aws_subnet.private1_1.id
}

resource "aws_route_table_association" "private_association_1_2" {
    route_table_id = aws_route_table.private_rt.id
    subnet_id      = aws_subnet.private1_2.id
}

resource "aws_route_table_association" "private_association_2_1" {
    route_table_id = aws_route_table.private_rt.id
    subnet_id      = aws_subnet.private2_1.id
}

resource "aws_route_table_association" "private_association_2_2" {
    route_table_id = aws_route_table.private_rt.id
    subnet_id      = aws_subnet.private2_2.id
}