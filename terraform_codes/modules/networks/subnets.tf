// ===========================================
// ----------------SUBNETS--------------------
// ===========================================

// front subnets
resource "aws_subnet" "public1" {
    vpc_id = aws_vpc.step_vpc.id
    cidr_block = "10.0.1.0/24"
    availability_zone = var.availability_zones[0]
    tags = {
        Name = "public-1"
        Group = "front"
    }
}

resource "aws_subnet" "public2" {
    vpc_id = aws_vpc.step_vpc.id
    cidr_block = "10.0.2.0/24"
    availability_zone = var.availability_zones[1]
    tags = {
        Name = "public-2"
        Group = "front"
    }
}

data "aws_subnet" "public1_data" {
    id = aws_subnet.public1.id
}


data "aws_subnet" "public2_data" {
    id = aws_subnet.public2.id  
}


// back subnets
resource "aws_subnet" "private1_1" {
    vpc_id = aws_vpc.step_vpc.id
    cidr_block = "10.0.3.0/24"
    availability_zone = var.availability_zones[0]
    tags =  {
        Name = "private-1_1"
        Group = "back"
    }
}


resource "aws_subnet" "private1_2" {
    vpc_id = aws_vpc.step_vpc.id
    cidr_block = "10.0.4.0/24"
    availability_zone = var.availability_zones[1]
    tags = {
        Name = "private-1_2"
        Group = "back"
    }
}


data "aws_subnet" "private1_1_data" {
    id = aws_subnet.private1_1.id
}

data "aws_subnet" "private1_2_data" {
    id = aws_subnet.private1_2.id
}

// database subnets
resource "aws_subnet" "private2_1" {
    vpc_id = aws_vpc.step_vpc.id
    cidr_block = "10.0.5.0/24"
    availability_zone = var.availability_zones[0]
    tags = {
        Name = "private-2_1"
        Group = "database"
    }
}

resource "aws_subnet" "private2_2" {
    vpc_id = aws_vpc.step_vpc.id
    cidr_block = "10.0.6.0/24"
    availability_zone = var.availability_zones[1]
    tags = {
        Name = "private-2_2"
        Group = "database"
    }
}

data "aws_subnet" "private2_1_data" {
    id = aws_subnet.private2_1.id
}

data "aws_subnet" "private2_2_data" {
    id = aws_subnet.private2_2.id
}