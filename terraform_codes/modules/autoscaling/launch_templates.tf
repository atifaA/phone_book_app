// front template
resource "aws_launch_template" "front_template" {

    name = "front_template"
    instance_type = var.aws_instance_type 
    image_id = var.ami_id

    instance_initiated_shutdown_behavior = "terminate"
    update_default_version = true

    key_name = var.key_pair_key_name

    network_interfaces {
      device_index = 0
      associate_public_ip_address = false

      security_groups = [
          var.security_group_ids.front_security_group,
          var.security_group_ids.ssh
      ]  

      subnet_id = var.subnet_ids.front[0]
      delete_on_termination = true
    }

    network_interfaces {
      device_index = 1
      associate_public_ip_address = false

      security_groups = [
          var.security_group_ids.front_security_group,
          var.security_group_ids.ssh
      ]  

      subnet_id = var.subnet_ids.front[1]
      delete_on_termination = true
    }
    tag_specifications {
      resource_type = "instance"

      tags = {
        Name = "front"
      }
    }


    # user_data = filebase64("")
}

//back template
resource "aws_launch_template" "back_template" {

    name = "back_template"
    instance_type = var.aws_instance_type 
    image_id = var.ami_id

    instance_initiated_shutdown_behavior = "terminate"
    update_default_version = true

    key_name = var.key_pair_key_name

    network_interfaces {
      device_index = 0
      associate_public_ip_address = false

      security_groups = [
          var.security_group_ids.ssh,
          var.security_group_ids.back_security_group
      ]

      subnet_id = var.subnet_ids.back[0]
    }

    network_interfaces {
      device_index = 1
      associate_public_ip_address = false

      security_groups = [
          var.security_group_ids.ssh,
          var.security_group_ids.back_security_group
      ]

      subnet_id = var.subnet_ids.back[1]
    }


    tag_specifications {
      resource_type = "instance"

      tags = {
        Name = "back"
      }
    }
}