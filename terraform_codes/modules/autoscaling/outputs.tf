output "autoscaling_group_names" {
    value = {
        front = aws_autoscaling_group.front_auto_scaling_group.name
        back  = aws_autoscaling_group.back_auto_scaling_group.name
    }
}

output "autoscaling_policy_arns" {
    value = {
        front_scale_in = aws_autoscaling_policy.policy_front_scale_in.arn
        front_scale_out = aws_autoscaling_policy.policy_front_scale_out.arn
        back_scale_in  = aws_autoscaling_policy.policy_back_scale_in.arn
        back_scale_out  = aws_autoscaling_policy.policy_back_scale_out.arn
    }
}