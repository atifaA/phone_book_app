resource "aws_autoscaling_group" "front_auto_scaling_group" {
    name = "front_auto_scaling_group"

    availability_zones = var.availability_zones
    desired_capacity = 2
    min_size = 2
    max_size = 10

    target_group_arns = [var.step_target_group_arns.front] 

    health_check_grace_period = 120
    protect_from_scale_in = false
    launch_template {
        id = aws_launch_template.front_template.id
        version = "$Latest"
    }

    tag {
        key = "auto_scaling_group"
        value = "front"
        propagate_at_launch = true
    }

}

resource "aws_autoscaling_group" "back_auto_scaling_group" {
    name = "back_auto_scaling_group"

    availability_zones = var.availability_zones
    desired_capacity = 2
    min_size = 2
    max_size = 10

    target_group_arns = [var.step_target_group_arns.back]

    health_check_grace_period = 120
    protect_from_scale_in = false
    launch_template {
        id = aws_launch_template.back_template.id
        version = "$Latest"
    }

    tag {
        key   = "auto_scaling_group"
        value = "back"
        propagate_at_launch = true
    }
}