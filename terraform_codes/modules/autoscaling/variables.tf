variable "aws_instance_type" {
    type = string
}

variable "ami_id" {
    type = string
}

variable "key_pair_key_name" {
    type = string
}

variable "security_group_ids" {
    type = object({
        web         = string
        ssh         = string
        front_security_group = string
        back_security_group  = string
    })
}

variable "subnet_ids" {
    type = object({
        front = list(string)
        back  = list(string)
    })
}

variable "availability_zones" {
    type = list
}

variable "step_target_group_arns" {
    type = object({
        front = string
        back  = string
    })
}
