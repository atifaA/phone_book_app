resource "aws_autoscaling_policy" "policy_front_scale_out" {
    name = "front_policy"
    autoscaling_group_name = aws_autoscaling_group.front_auto_scaling_group.name

    adjustment_type = "ChangeInCapacity" 
    policy_type = "StepScaling"

    step_adjustment {
      scaling_adjustment = 1
      metric_interval_lower_bound = 0 
    }       
}

resource "aws_autoscaling_policy" "policy_front_scale_in" {
    name = "front_policy"
    autoscaling_group_name = aws_autoscaling_group.front_auto_scaling_group.name

    adjustment_type = "ChangeInCapacity"
    policy_type = "StepScaling"

    step_adjustment {
      scaling_adjustment = -1
      metric_interval_upper_bound = 0
    }       
}


resource "aws_autoscaling_policy" "policy_back_scale_out" {
    name = "back_policy"
    autoscaling_group_name = aws_autoscaling_group.back_auto_scaling_group.name

    adjustment_type = "ChangeInCapacity"
    policy_type = "StepScaling"

    step_adjustment {
      scaling_adjustment = 1
      metric_interval_lower_bound = 0
    }
}

resource "aws_autoscaling_policy" "policy_back_scale_in" {
    name = "back_policy"
    autoscaling_group_name = aws_autoscaling_group.back_auto_scaling_group.name

    adjustment_type = "ChangeInCapacity"
    policy_type = "StepScaling"

    step_adjustment {
      scaling_adjustment = -1
      metric_interval_upper_bound = 0
    }
}