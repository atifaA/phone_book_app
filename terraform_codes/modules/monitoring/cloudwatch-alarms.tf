resource "aws_cloudwatch_metric_alarm" "front_scale_out_alarm" {
    alarm_name = "front_scale_out_alarm"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = 2
    metric_name = "RequestCount"
    namespace =  "AWS/ApplicationELB"
    period = "60"
    statistic = "SampleCount"
    threshold = "25" 

    dimensions = {
      AutoScalingGroupName = var.autoscaling_group_names.front
    }

    alarm_description = "request count alarm for front autoscaling group per minute"
    alarm_actions = [var.autoscaling_policy_arns.front_scale_out]
}

resource "aws_cloudwatch_metric_alarm" "front_scale_in_alarm" {
    alarm_name = "front_scale_in_alarm"
    comparison_operator = "LessThanOrEqualToThreshold" 
    evaluation_periods = 2
    metric_name = "RequestCount"
    namespace =  "AWS/ApplicationELB"
    period = "60"
    statistic = "SampleCount"
    threshold = "10" # 10 requests per minute

    dimensions = {
      AutoScalingGroupName = var.autoscaling_group_names.front
    }

    alarm_description = "This metric monitors the request per minute to front auto_scaling_group"
    alarm_actions = [var.autoscaling_policy_arns.front_scale_in]
}


resource "aws_cloudwatch_metric_alarm" "back_scale_out_alarm" {
    alarm_name = "back_scale_out_alarm"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = 2
    metric_name = "RequestCount"
    namespace =  "AWS/ApplicationELB"
    period = "60"
    statistic = "SampleCount"
    threshold = "30" 

    dimensions = {
      AutoScalingGroupName = var.autoscaling_group_names.back
    }

    alarm_description = "request count alarm for back autoscaling group per minute"
    alarm_actions = [var.autoscaling_policy_arns.back_scale_out]
}

resource "aws_cloudwatch_metric_alarm" "back_scale_in_alarm" {
    alarm_name = "back_scale_in_alarm"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = 2
    metric_name = "RequestCount"
    namespace =  "AWS/ApplicationELB"
    period = "60"
    statistic = "SampleCount"
    threshold = "10" 

    dimensions = {
      AutoScalingGroupName = var.autoscaling_group_names.back 
    }

    alarm_description = "This metric monitors the request per minute to back auto_scaling_group"
    alarm_actions = [var.autoscaling_policy_arns.back_scale_in]
}