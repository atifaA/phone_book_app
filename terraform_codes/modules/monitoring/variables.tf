variable "autoscaling_group_names" {
    type = object({
        front = string
        back  = string
    })
}

variable "autoscaling_policy_arns" {
    type = object({
        front_scale_in  = string
        front_scale_out = string
        back_scale_in   = string
        back_scale_out  = string
    })
}